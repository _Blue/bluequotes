#! /usr/bin/python3
from Quotes.models import Quote
from .logged_request import *
from .logout_handler import *
from .request_check import *
from django.http import *


class create_handler(logged_request):
    def get_impl(self, request, user: User):
        return render(request, 'create.html')

    def post_impl(self, request, user: User):
        check_args(request, ('content', 'title'))

        # Search for a quote with that title
        if any(True for e in Quote.objects.all() if e.title == request.POST['title']):
            return HttpResponse('Please select another title')

        # Create quote
        quote = Quote(author=user, content=request.POST['content'], title=request.POST['title'])

        # Save quote in DB
        quote.save()
        return redirect('/')
