from Quotes.models import Quote
from .logged_request import *
from . import logout_handler
from django.http import *


class home_handler(logged_request):
    def get_impl(self, request, user):
        latest = Quote.objects.order_by('-created')
        return render(request, 'home.html', {'latest': latest})
