from django.contrib.auth.models import AnonymousUser
from django.core.exceptions import SuspiciousOperation
from django.core.urlresolvers import resolve
from django.test import Client

from .login_handler import *
from .authentication import *
from django.test import TestCase, RequestFactory
from .signup_handler import *
from .urls import *
from .quote_handler import *
import time


class authentication_test(TestCase):
    def test_valid_signup_token(self):
        token = signup_token('koin@lol.fr')
        self.assertEqual(signup_token_email(token), 'koin@lol.fr')

        token = signup_token('')
        self.assertEqual(signup_token_email(token), '')

    def test_invalid_signup_token(self):
        # Empty token
        self.assertRaises(InvalidToken, signup_token_email, token='')

        # Bad signature
        token = signup_token('lol@lol.fr', 'bad_secret')
        self.assertRaises(InvalidToken, signup_token_email, token=token)

        # Bad signature
        token = signup_token('lol@lol.fr')
        token = token[:len(token) - 1]
        self.assertRaises(InvalidToken, signup_token_email, token=token)

        # Bad base64
        self.assertRaises(InvalidToken, signup_token_email, token="*\"")

    def test_valid_token(self):
        token = create_token(1)
        self.assertEqual(read_token(token), 1)

        token = create_token(65536)
        self.assertEqual(read_token(token), 65536)

    def test_invalid_token(self):
        self.assertRaises(InvalidToken, read_token, token='')
        self.assertRaises(InvalidToken, read_token, token='*\"')
        self.assertRaises(InvalidToken, read_token, token='abcd')

        token = create_token(1, 30, 'bad_secret')
        self.assertRaises(InvalidToken, read_token, token=token)

        token = create_token(1)
        token = token[:len(token) - 1]
        self.assertRaises(InvalidToken, read_token, token=token)

        # test token expiration
        token = create_token(1, 1)  # create a 1 second token
        time.sleep(2)
        self.assertRaises(InvalidToken, read_token, token=token)


class request_test(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def run_request(self, uri: str, code: int, method="GET", post_args={}, cookies={}):
        client = Client()

        for k in cookies:
            client.cookies[k] = cookies[k]

        if method == "GET":
            response = client.get(uri)
        else:
            response = client.post(uri, data=post_args)

        self.assertEqual(response.status_code, code)
        return response


class signup_test(request_test):
    def test_signup_valid(self):
        token = authentication.signup_token('my_email@mail.com')
        self.run_request('/signup?token=' + token, 200)

    def test_signup_post_valid(self):
        token = authentication.signup_token('my_email@mail.com')
        self.run_request('/signup', 302, method='POST',
                         post_args={'token': token, 'hash': 'abcd', 'name': 'tester'})

        user = next(e for e in User.objects.all() if e.email == 'my_email@mail.com')
        self.assertIsNotNone(user)
        self.assertEqual(user.email, "my_email@mail.com")
        self.assertEqual(user.name, "tester")
        self.assertEqual(user.password_hash, "abcd")

    def test_signup_post_invalid(self):
        # Duplicated account
        token = authentication.signup_token('duplicated@mail.com')
        self.run_request('/signup', 302, method='POST',
                         post_args={'token': token, 'hash': 'abcd', 'name': 'tester'})
        self.run_request('/signup', 409, method='POST',
                         post_args={'token': token, 'hash': 'abcd', 'name': 'tester'})

        self.run_request('/signup', 400, method='POST',
                         post_args={'hash': 'abcd', 'name': 'tester'})

        # Missing parameters
        self.run_request('/signup', 400, method='POST',
                         post_args={'token': token, 'name': 'tester'})

        self.run_request('/signup', 400, method='POST',
                         post_args={'token': token, 'hash': 'abcd', })

        self.run_request('/signup', 400, method='POST')

    def test_signup_invalid(self):
        self.run_request('/signup?token=abcd', 400)
        self.run_request('/signup?token=', 400)
        self.run_request('/signup', 400)

    def get_handler(self):
        return signup_handler


class login_test(request_test):
    def execute(self, args: dict, sucess: bool):
        request = self.factory.get("/login")
        request.user = AnonymousUser()
        request.method = "POST"
        request.POST = args

        response = login_handler.as_view()(request)
        self.assertEqual(response.status_code, 302)
        self.assertEqual('token' in response.cookies, sucess)

    def test_valid_login(self):
        # Simple test
        password_hash = hashlib.sha256('passwordlogin@tester.com'.encode('utf-8')).hexdigest()
        user = User(name='login', password_hash=password_hash, email='login@tester.com')
        user.save()

        login_hash = login_handler.compute_hash(user, 'salt')

        self.execute({'hash': login_hash, 'salt': 'salt', 'email': 'login@tester.com'}, True)

        # Complex password
        password_hash = hashlib.sha256('12**==éé²²²??éé??\"\'@login_complex@tester.com'.encode('utf-8')).hexdigest()
        user = User(name='login', password_hash=password_hash, email='login_complex@tester.com')
        user.save()

        login_hash = login_handler.compute_hash(user, 'salt')

        self.execute({'hash': login_hash, 'salt': 'salt', 'email': 'login_complex@tester.com'}, True)

    def test_invalid_login(self):
        # Bad hash value
        password_hash = hashlib.sha256('passwordlogin_fail@tester.com'.encode('utf-8')).hexdigest()
        user = User(name='login', password_hash=password_hash, email='login_fail@tester.com')
        user.save()

        login_hash = login_handler.compute_hash(user, 'salt_changed')

        self.execute({'hash': login_hash, 'salt': 'salt', 'email': 'login_fail@tester.com'}, False)

        # Missing hash
        self.execute({'salt': 'salt', 'email': 'login_fail@tester.com'}, False)

        # Missing salt
        self.execute({'hash': login_hash, 'email': 'login_fail@tester.com'}, False)

        # Missing email
        self.execute({'hash': login_hash, 'salt': 'salt'}, False)

        # Nothing
        self.execute({}, False)

        # Bad email
        self.execute({'hash': login_hash, 'salt': 'salt', 'email': 'login_doesntexist@tester.com'}, False)


class home_test(request_test):
    def test_valid_home(self):
        user = User(name='home', email='home@tester.com', password_hash='12')
        user.save()
        token = authentication.create_token(user.id)
        self.run_request("/", 200, cookies={"token": token})

    def test_invalid_home(self):
        self.run_request("/", 302, cookies={"token": "abcd"})
        self.run_request("/", 302)


class quote_test(request_test):
    def setUp(self):
        super().setUp()
        self.user = User(name='quote', email='quote@tester.com', password_hash='12')
        self.user.save()
        self.token = authentication.create_token(self.user.id)
        self.quote = Quote(author=self.user, content='koin', title='title')
        self.quote.save()

    def test_valid_quote(self):
        self.run_request("/quote/" + str(self.quote.id), 200, cookies={'token': self.token})

    def test_invalid_quote(self):
        self.run_request("/quote/" + str(self.quote.id), 302, cookies={})
        self.run_request("/quote/12121212", 404, cookies={"token": self.token})
        self.run_request("/quote/0", 404, cookies={"token": self.token})
        self.run_request("/quote", 404, cookies={"token": self.token})
        self.run_request("/quote/", 404, cookies={"token": self.token})
        self.run_request("/quote/a", 400, cookies={"token": self.token})
        self.run_request("/quote/\"(?&!", 404, cookies={"token": self.token})


class create_test(request_test):
    def setUp(self):
        super().setUp()
        self.user = User(name='create', email='create@tester.com', password_hash='12')
        self.user.save()
        self.token = authentication.create_token(self.user.id)
        self.cookies = {"token": self.token}

    def test_valid_create(self):
        response = self.run_request("/create", 302, method="POST",
                                     post_args={"title": "test quote", "content": "test content"}, cookies=self.cookies)

        self.assertEquals(response.url, '/')

    def test_invalid_create(self):
        response = self.run_request("/create", 302, method="POST",
                                     post_args={"title": "test quote", "content": "test content"}, cookies={})
        self.assertEqual(response.url, '/login')

        response = self.run_request("/create", 302, method="POST",
                                     post_args={"title": "test quote", "content": "test content"}, cookies={'token': 'koin'})
        self.assertEqual(response.url, '/login')

        self.run_request("/create", 400, method="POST",
                         post_args={"content": "test content"}, cookies=self.cookies)

        self.run_request("/create", 400, method="POST",
                         post_args={"content": "test content"}, cookies=self.cookies)

        self.run_request("/create", 400, method="POST", post_args={}, cookies=self.cookies)

        #Test duplicate title:
        response = self.run_request("/create", 302, method="POST",
                                     post_args={"title": "duplicate", "content": "test content"}, cookies=self.cookies)

        self.assertEquals(response.url, '/')

        self.run_request("/create", 200, method="POST", #200 for basic error message
                         post_args={"title": "duplicate", "content": "test content"}, cookies=self.cookies)