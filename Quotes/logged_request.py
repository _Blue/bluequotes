import django.views
from django.http import *
from django.shortcuts import *
from Quotes.models import User
from .authentication import *

class logged_request(django.views.View):
    def get(self, request, **args):
        try:
            user = self.get_user_from_token(request)
        except Exception as e:
            return redirect('/login')
        return self.get_impl(request, user, **args)

    def post(self, request):
        try:
            user = self.get_user_from_token(request)
        except:
            return redirect('/login')
        return self.post_impl(request, user)

    def get_user_from_token(self, request) -> User:
        cookie = request.COOKIES['token']
        user_id = read_token(cookie)
        return get_object_or_404(User, id=user_id)

    def get_impl(self, request, user):
        return HttpResponseBadRequest("Bad method")

    def post_impl(self, request, user):
        return HttpResponseBadRequest("Bad method")
