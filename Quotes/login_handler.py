#! /usr/bin/python3

import os
import django.views
from django.shortcuts import *

from Quotes.models import User
from . import models
from .authentication import *
import base64

SALT_LENGTH=64

class login_handler(django.views.View):
    def get(self, request):
        salt = base64.b64encode(os.urandom(SALT_LENGTH))
        return render(request, 'login.html', {'salt': salt})

    def post(self, request):
        try:
            email = request.POST['email']
            hash = request.POST['hash']
            salt = request.POST['salt']
            user = get_object_or_404(models.User, email=email)
        except:
            return redirect('/login')

        correct_hash = login_handler.compute_hash(user, salt)
        if hash != correct_hash:
            return redirect('/login')

        response = HttpResponseRedirect('/')
        response.set_cookie('token', create_token(user.id))

        return response

    def compute_hash(user: User, salt: str):
        return hashlib.sha256((user.password_hash + salt).encode('utf-8')).hexdigest()
