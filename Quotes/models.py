from django.db import models


class User(models.Model):
    name = models.CharField(max_length=16)
    password_hash = models.CharField(max_length=128)
    email = models.CharField(max_length=128, unique = True)

class Quote(models.Model):
    content = models.CharField(max_length=4096)
    title = models.CharField(max_length=128)
    created = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
