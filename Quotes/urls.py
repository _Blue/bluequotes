from django.conf.urls import include, url
from django.contrib import admin
import django.contrib.auth.views
from  .logout_handler import *
from .home_handler import *
from .login_handler import *
from .signup_handler import *
from .create_handler import *
from .quote_handler import *

urlpatterns = [
        url(r'^admin/', admin.site.urls),
        url(r'^$', home_handler.as_view()),
        url(r'^quote/(?P<id>\w+)$', quote_handler.as_view()),
        url(r'^create$', create_handler.as_view()),
        url(r'^login$', login_handler.as_view()),
        url(r'^signup$', signup_handler.as_view()),
        url(r'^logout$', logout_handler.as_view()),
        ]
