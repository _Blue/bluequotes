from django.core.exceptions import SuspiciousOperation

from Quotes.models import Quote
from .logged_request import *

class quote_handler(logged_request):
    def get_impl(self, request, user, id):
        try:
            id = int(id)
        except:
            raise SuspiciousOperation()

        quote = get_object_or_404(Quote, id=id)
        try:
            next_id = Quote.objects.get(id=int(id) + 1).id
        except:
            next_id = 0
        return render(request, 'quote.html', {'quote': quote,
                                              'previous': quote.id - 1,
                                              'next': next_id})


