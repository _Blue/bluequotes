#! /usr/bin/python3
import os
import hashlib
import time
import base64
import traceback
import BlueQuotes.settings

#session key is used to sign tokens
SESSION_KEY = str(os.urandom(64))

#Secret key is used to sign the signup token
SECRET_KEY = BlueQuotes.settings.SECRET_KEY

class InvalidToken(Exception):
    pass


# Create a token, contains user id, timestamp and signature
def create_token(user_id: int, duration: int = 3600 * 24, secret=SESSION_KEY) -> str:
    return seal(str(user_id) + ';' + str(int(time.time()) + duration), secret)


# Create a signup token, contains email address and signature
def signup_token(email: str, secret=SECRET_KEY) -> str:
    return seal(email, secret)


# Retrieve email from signup token
def signup_token_email(token: str) -> str:
    try:
        plain = base64.b64decode(token)
        values = plain.decode('UTF-8').split(';')

        if not signature_valid(values[0], values[1], SECRET_KEY):
            raise InvalidToken('Token signature is incorrect')
        return values[0]
    except InvalidToken as e:
        raise e
    except Exception as e:
        raise InvalidToken('Token format is invalid')

# Sign a values, format is base64(value):signature
def seal(value: str, secret=SESSION_KEY) -> str:
    blob = bytes(value + ';' + hashlib.sha256(value.encode('UTF-8') + secret.encode('UTF-8')).hexdigest(), 'UTF-8')
    return base64.b64encode(blob).decode('utf-8')

# Get user ID from token, verifies signature, and timestamp
def read_token(token: str) -> int:
    try:
        plain = base64.b64decode(token)
        values = plain.decode('UTF-8').split(';')

        if len(values) != 3:
            raise InvalidToken('invalid token')

        if not signature_valid(values[0] + ';' + values[1], values[2], SESSION_KEY):
            raise InvalidToken('Token signature is incorrect')

        if int(values[1]) < time.time():
            raise InvalidToken('Token is expired (expired: ' + values[1] + ')')

        return int(values[0])
    except InvalidToken as e:
        raise e
    except Exception as e:
        raise InvalidToken('Token format is invalid')

def signature_valid(content: str, signature: str, secret: str) -> bool:
    return hashlib.sha256(content.encode('utf-8') + secret.encode('utf-8')).hexdigest() == signature
