#! /usr/bin/python3

from urllib import parse

import django.views
from django.shortcuts import *

from Quotes.models import User
from . import authentication
from . import request_check


class signup_handler(django.views.View):
    def get(self, request):
        parsed = parse.urlparse(request.get_full_path())
        try:
            token = parse.parse_qs(parsed.query)['token'][0]
            return render(request, 'signup.html', {'email': authentication.signup_token_email(token),
                                                   'token': token})
        except KeyError:
            return HttpResponse('Missing token', status=400)
        except authentication.InvalidToken:
            return HttpResponse('Invalid token', status=400)

    def post(self, request):
        request_check.check_args(request, ('token', 'hash', 'name'))
        try:
            email = authentication.signup_token_email(request.POST['token'])
            if request_check.empty_password_hash(email, request.POST['hash']):
                return HttpResponse('Use of empty password is prohibited')
            hash = request.POST['hash']
            nickname = request.POST['name']
            if User.objects.filter(email=email):
                return HttpResponse('This email is already taken', status=409)
        except authentication.InvalidToken:
            return HttpResponse('Bad token')
        except Exception as e:
            return redirect('/signup?token=' + request.POST['token'])

        user = User(email=email, password_hash=hash, name=nickname)
        user.save()
        return redirect('/login')
