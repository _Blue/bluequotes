#! /usr/bin/python3
from django.core.exceptions import SuspiciousOperation
import hashlib

# Verify that all the arguments are present, and not empty
def check_args(request, args: list) -> bool:
    for arg in args:
        if (not arg in request.POST) or not request.POST[arg]:
            raise SuspiciousOperation("Missing or empty argument")

def empty_password_hash(email, hash_value) -> bool:
    return hashlib.sha256(email.encode('UTF-8')).hexdigest() == hash_value
