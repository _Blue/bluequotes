from django.shortcuts import *

from Quotes.logged_request import logged_request


class logout_handler(logged_request):
    def get_impl(self, request, user):
        response = HttpResponseRedirect('/')
        response.delete_cookie('token')
        return response
